#!/bin/bash

set -euo pipefail

mytmpname=$(basename $0)
mytmp=$(mktemp -d /tmp/$mytmpname-XXXXX)

function cleanup() {
    echo "Removing $mytmp"
    rm -rf $mytmp
}
trap cleanup EXIT


cd $mytmp
git init repo
cd repo
git remote add git https://gitlab.com/git-vcs/git.git
git remote add git-gitlab-ci https://gitlab.com/git-vcs/git-gitlab-ci.git
git remote add git-ci https://gitlab.com/git-vcs/git-ci.git
git remote set-url git-ci --push git@gitlab.com:git-vcs/git-ci.git

git fetch --all --no-tags

for branch in $(git for-each-ref --format='%(refname)' | grep refs/remotes/git-ci/ | sed 's!.*/!!')
do
    git checkout -t git/$branch
    git merge --allow-unrelated-histories --no-edit git-gitlab-ci/master
    git push git-ci HEAD -f
done
