#!/bin/bash

# Testing:
# Solaris:
#  OPT_DEBUG=1 CI_JOB_NAME='gcc119' ci/gitlab/run-on-gcc-farm.sh
#  OPT_DEBUG=1 CI_JOB_NAME='gcc210' OPT_TESTS='t[0-9]*.sh' OPT_JOBS=10 ci/gitlab/run-on-gcc-farm.sh
# OpenBSD:
#  OPT_DEBUG=1 CI_JOB_NAME='gcc302' OPT_TESTS='t13[0-9]*.sh' ci/gitlab/run-on-gcc-farm.sh
# General:
#  OPT_DEBUG=1 CI_JOB_NAME='gcc112' CI_GIT_COMMIT_SHA=git/pu OPT_JOBS=20 ci/gitlab/run-on-gcc-farm.sh
#  OPT_DEBUG=1 CI_JOB_NAME='gcc112' CI_GIT_COMMIT_SHA=git/next OPT_TESTS='t00[0-9]*.sh' OPT_JOBS=20 ci/gitlab/run-on-gcc-farm.sh

set -eo pipefail

if test -z "$OPT_REMOTE"
then
    if test -n "$CI_JOB_NAME"
    then
        CI_GIT_GCC_HOST=$(echo "$CI_JOB_NAME" | cut -d ' ' -f 1)
    else
        CI_GIT_GCC_HOST=gcc112
    fi

    if test -n "$CI_GIT_COMMIT_SHA"
    then
        rev_parse=$(git rev-parse $CI_GIT_COMMIT_SHA)
        echo "Going to test '$CI_GIT_COMMIT_SHA' (parsed as '$rev_parse')"
        CI_GIT_COMMIT_SHA=$rev_parse
    elif test "$(git remote get-url origin 2>/dev/null)" = "git@gitlab.com:git-vcs/git-gitlab-ci.git"
    then
        # I'm in the git-gitlab-ci repo itself running an ad-hoc
        # test. Using ls-remote here instead of 'git rev-parse
        # git/master' is intentional. If Junio's pushed out a new
        # version I want to re-fetch.
        CI_GIT_COMMIT_SHA=$(git ls-remote https://gitlab.com/git-vcs/git.git refs/heads/master | cut -f1)
        if ! git cat-file -e $CI_GIT_COMMIT_SHA 2>/dev/null
        then
            echo "Don't have $CI_GIT_COMMIT_SHA here. Fetching git.git"
            git fetch git
        fi
    else
        # The merge we create always has the git.git push as the LHS
        # of the merge.
        CI_GIT_COMMIT_SHA=$(git rev-parse HEAD^)
    fi

    if ! git rev-parse --verify $CI_GIT_COMMIT_SHA >/dev/null 2>&1
    then
        # In my test repo
        git remote add git https://gitlab.com/git-vcs/git.git
        git config remote.origin.tagOpt --no-tags
        git fetch git
    fi
    ARCHIVE=git-$CI_GIT_COMMIT_SHA
    ARCHIVE_TAR=$ARCHIVE.tar
    git archive \
        --format=tar \
        --output=$ARCHIVE_TAR \
        --prefix=git-$CI_GIT_COMMIT_SHA/ \
        $CI_GIT_COMMIT_SHA
    gzip -f $ARCHIVE_TAR
    ARCHIVE_TAR_GZ=$ARCHIVE_TAR.gz

    PORT=22
    case $CI_GIT_GCC_HOST in
        gcc300)
            PORT=2400
            ;;
        gcc302)
            PORT=2402
            ;;
        gcc303)
            PORT=2403
            ;;
    esac

    PATH_PREFIX=
    case $CI_GIT_GCC_HOST in
        gcc119)
            PATH_PREFIX=/scratch/avar/
            ;;
    esac

    if test -n "$PATH_PREFIX"
    then
        ssh -o Port=$PORT -o StrictHostKeyChecking=no avar@$CI_GIT_GCC_HOST.fsffrance.org \
            "mkdir -p $PATH_PREFIX"
    fi

    if test -n "$OPT_DEBUG" && ssh -o Port=$PORT -o StrictHostKeyChecking=no avar@$CI_GIT_GCC_HOST.fsffrance.org "test -e $PATH_PREFIX$ARCHIVE"
    then
        echo "Already have $PATH_PREFIX$ARCHIVE on $CI_GIT_GCC_HOST.fsffrance.org (debugging)"
    else
        echo "Copying $ARCHIVE_TAR_GZ to $CI_GIT_GCC_HOST.fsffrance.org:$PATH_PREFIX"
        scp -o Port=$PORT -o StrictHostKeyChecking=no $ARCHIVE_TAR_GZ avar@$CI_GIT_GCC_HOST.fsffrance.org:$PATH_PREFIX
    fi
    ssh -o Port=$PORT -o StrictHostKeyChecking=no avar@$CI_GIT_GCC_HOST.fsffrance.org \
        "\
        OPT_REMOTE=\"$CI_GIT_GCC_HOST\" \
        OPT_DEBUG=\"$OPT_DEBUG\" \
        OPT_TESTS=\"$OPT_TESTS\" \
        OPT_JOBS=\"$OPT_JOBS\" \
        CI_GIT_COMMIT_SHA=\"$CI_GIT_COMMIT_SHA\" \
        PATH_PREFIX=\"$PATH_PREFIX\" \
        nice -n 19 bash -" \
        <$0
else
    # Config
    TAR=tar
    JOBS=1
    case $OPT_REMOTE in
        gcc10)
            JOBS=2
            ;;
        gcc110)
            JOBS=2
            ;;
        gcc110|gcc111|gcc112|gcc135)
            JOBS=4
            ;;
        gcc119)
            JOBS=4
            ;;
        gcc202)
            JOBS=4
            ;;
        gcc210|gcc211)
            JOBS=2
            PATH=/opt/csw/gnu:$PATH:/opt/csw/bin
            TAR=gtar
            ;;
    esac

    # Options (some override config)
    if test -n "$OPT_JOBS"
    then
        JOBS=$OPT_JOBS
    fi

    # Skip known failures. TODO: Re-run failing tests with -v -x
    GIT_SKIP_TESTS=
    case $OPT_REMOTE in
        gcc16)
            GIT_SKIP_TESTS+='t1300 ' # https://public-inbox.org/git/20181127164253.9832-1-avarab@gmail.com/T/#u
            GIT_SKIP_TESTS+=$(echo t9115.{11..12})
            GIT_SKIP_TESTS+=' '
            ;;
        gcc110)
            GIT_SKIP_TESTS+='t5541.33 '
            GIT_SKIP_TESTS+='t5551.25 '
            ;;
        gcc119)
            GIT_SKIP_TESTS+=$(echo t0000.{6,{10..13},{30..32},43})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+='t0001.35 '
            GIT_SKIP_TESTS+='t0021.18 '
            GIT_SKIP_TESTS+=$(echo t0028.{1..13})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t0301.{29..30})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t0410.{{5..7},{12..16},18,22})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t1300.{{117..118},{120..125},128,{136..140},155,{157..158},{160..165}})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t1400.{170..171})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t1450.{21,55,66,{71..72},{74..75}})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+='t3210.30 '
            GIT_SKIP_TESTS+=$(echo t3900.{17,25,{27..29}})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+='t4014 '
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t4020.{18,23})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t4150.{67..68})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t4205.{57..58})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t5004.{9..10})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t5100.{5,10,21})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+='t5310.43 '
            GIT_SKIP_TESTS+='t5316.3 '
            GIT_SKIP_TESTS+='t5410.3 '
            GIT_SKIP_TESTS+='t5700 '
            GIT_SKIP_TESTS+='t5702 '
            GIT_SKIP_TESTS+='t5703 '
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+='t5811 '
            GIT_SKIP_TESTS+=$(echo t6300.{3,{5..12},{15..16},{19..24},{27..32},{37..38},{76..77},{142..143},147,{151..153},{191..200},{203..205},207})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+='t7508 '
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t8001.{53..54})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t8002.{{53..54},{117..118}})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t8005.{2..4})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t8012.{{53..54},{107..109}})
            GIT_SKIP_TESTS+=' '
            ;;
        gcc210|gcc211)
            GIT_SKIP_TESTS+=$(echo t0028.{1..14})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+='t1308.23 '
            GIT_SKIP_TESTS+=$(echo t3900.{{15..17},{23..25},{27..28}})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t3901.{{2..3},{6..7},9,11,{14..20}})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t4041.{{5..7},{9..10}})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t4059.{4..8})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+='t4060.8 '
            GIT_SKIP_TESTS+='t4201.11 '
            GIT_SKIP_TESTS+=$(echo t4210.{2..5})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t4205.{{12..14},17,{19..20},22,{24..30},{32..33},{36..37},39,{41..42},44,{46..47},{51..52}})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t5100.{{5..6},10,12,{21..31}})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+='t5500 '
            GIT_SKIP_TESTS+=$(echo t5545.{1..10})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+='t5562.15 '
            GIT_SKIP_TESTS+=$(echo t6006.{{9..10},12,{43..45},{48..52}})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+='t7102.2 '
            GIT_SKIP_TESTS+=$(echo t8005.{2..4})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t9350.{4,{13..14},{18..19},{31..32}})
            GIT_SKIP_TESTS+=' '
            test $OPT_REMOTE = 'gcc211' && GIT_SKIP_TESTS+='t9350.32 '
            ;;
        gcc300)
            GIT_SKIP_TESTS+=$(echo t0000.{{5..32},43})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+=$(echo t0301.{29..30})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+='t1308.23 '
            GIT_SKIP_TESTS+=$(echo t5004.{9..10})
            GIT_SKIP_TESTS+=' '
            GIT_SKIP_TESTS+='t5003 '
            ;;
        gcc302)
            GIT_SKIP_TESTS+='t1308.23 '
            GIT_SKIP_TESTS+=$(echo t5004.{9..10})
            GIT_SKIP_TESTS+=' '
            ;;
    esac
    export GIT_SKIP_TESTS
    if test -n "$GIT_SKIP_TESTS"
    then
        echo "Will be skipping tests: $GIT_SKIP_TESTS"
    fi

    # Cleanup setup
    function cleanup() {
        echo "Removing git-$CI_GIT_COMMIT_SHA*"
        cd $PATH_PREFIX
        rm -rf \
           ~/git-$CI_GIT_COMMIT_SHA \
           ~/git-$CI_GIT_COMMIT_SHA.tar \
           ~/git-$CI_GIT_COMMIT_SHA.tar.gz
    }
    if test -n "$OPT_DEBUG"
    then
        echo "Am debbugging. Will leave behind git-$CI_GIT_COMMIT_SHA*"
    else
        trap cleanup EXIT
    fi

    # Notice
    echo "Testing '$CI_GIT_COMMIT_SHA' on '$OPT_REMOTE' with '-j$JOBS'"
    cd $PATH_PREFIX

    # Setup
    if test -z "$OPT_DEBUG" -a -d git-$CI_GIT_COMMIT_SHA
    then
        echo "Removing existing git-$CI_GIT_COMMIT_SHA directory. Left over from OPT_DEBUG=1 run or stopped gitlab-runner?"
        rm -rf git-$CI_GIT_COMMIT_SHA
    fi

    if ! test -d git-$CI_GIT_COMMIT_SHA
    then
        # Skip this if OPT_DEBUG=1, in which case this has likely been
        # unpacked already.
        gzip -f -d git-$CI_GIT_COMMIT_SHA.tar.gz
        # 'z' in tar isn't portable on e.g. AIX.
        $TAR xf git-$CI_GIT_COMMIT_SHA.tar
    fi
    cd git-$CI_GIT_COMMIT_SHA

    # Configure
    >config.mak
    case $OPT_REMOTE in
        gcc10|gcc12|gcc16|gcc20|gcc68|gcc75|gcc114|gcc116|gcc135|gcc202):
            echo 'NO_CURL=UnfortunatelyNot' >>config.mak
            ;;
        gcc111|gcc119)
            echo 'CC=xlc' >>config.mak
            echo 'CFLAGS=-g -O2 -qmaxmem=32768' >>config.mak
            echo 'NO_CURL=UnfortunatelyNot' >>config.mak
            test $OPT_REMOTE = 'gcc111' && echo 'NO_GETTEXT=HaveNoMsgfmt' >>config.mak
            ;;
        gcc210|gcc211)
            test $OPT_REMOTE = 'gcc210' && echo 'CC=/opt/developerstudio12.5/bin/suncc' >>config.mak
            test $OPT_REMOTE = 'gcc211' && echo 'CC=/opt/developerstudio12.5/bin/suncc' >>config.mak

            test $OPT_REMOTE = 'gcc210' && echo 'NO_OPENSSL=UnfortunatelyNot' >>config.mak
            test $OPT_REMOTE = 'gcc210' && echo 'NO_CURL=UnfortunatelyNot' >>config.mak

            echo 'NO_ICONV=HasLinkerErrors' >>config.mak
            echo 'NO_GETTEXT=HaveNoMsgfmt' >>config.mak
            ;;
        gcc300)
            echo 'PERL_PATH=/usr/pkg/bin/perl' >>config.mak
            ;;
        gcc303)
            echo 'PYTHON_PATH=/usr/bin/python' >>config.mak
            ;;
    esac

    # Build
    if gmake --version >/dev/null 2>&1
    then
        gmake -j$JOBS
    else
        make -j$JOBS
    fi

    # Test
    if test -z "$OPT_TESTS"
    then
        OPT_TESTS='t[0-9]*.sh'
    fi
    cd t
    case $OPT_REMOTE in
        gcc12)
            TEST_NO_MALLOC_CHECK=YesBecauseOfMallocUsingDebuggingHooksStderrSpam prove $OPT_TESTS
            ;;
        gcc210|gcc211)
            echo "Doing parallel testing via /bin/bash with -j$JOBS $OPT_TESTS"
            prove --exec /bin/bash -j$JOBS $OPT_TESTS
            ;;
        gcc300)
            echo "Doing parallel testing via /usr/pkg/bin/bash with -j$JOBS $OPT_TESTS"
            prove --exec /usr/pkg/bin/bash  -j$JOBS $OPT_TESTS
            ;;
        *)
            if prove --help 2>&1 | grep -q jobs
            then
                echo "Doing parallel testing with -j$JOBS $OPT_TESTS"
                prove -j$JOBS $OPT_TESTS
            else
                echo "Doing parallel testing with -j$JOBS $OPT_TESTS"
                prove $OPT_TESTS
            fi
            ;;
    esac
fi
